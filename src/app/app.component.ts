import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as shape from 'd3-shape';

interface IApiResponse {
    data: { [key: string]: Array<[number, string, string, string]> };
    lastUpdatedAtSource: string;
    lastUpdatedAtApify: string;
    readMe: string;
}

interface IChartMultiSeries {
    name: string;
    series: Array<IChartSerieItem>;
}

interface IChartSerieItem {
    name: string | Date;
    value: number;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    public data: IApiResponse;
    public curve = shape.curveNatural;

    private dateFormat = new Intl.DateTimeFormat('cs', { year: 'numeric', month: 'numeric', day: '2-digit' });

    public newCasesPerDayChartData: Array<IChartMultiSeries> = [];
    public newCasesCummulativeChartData: Array<IChartMultiSeries> = [];
    public newCasesCummulativeLogaritmicChartData: Array<IChartMultiSeries> = [];

    public areasChartData: Array<IChartSerieItem> = [];
    public infectionSourceChartData: Array<IChartSerieItem> = [];
    public ageChartData: Array<IChartSerieItem> = [];
    public genderChartData: Array<IChartSerieItem> = [];

    public areas: { [key: string]: number } = {};
    public infectionSources: { [key: string]: number } = {};
    public age: { [key: string]: number } = {};
    public gender: { [key: string]: number } = {};

    public selectedArea = '';

    constructor(private readonly http: HttpClient) {}

    setNewCasesArea() {
        this.newCasesPerDayChartData = [
            {
                name: 'Nové případy za den',
                series: Object.entries(this.data.data).map(([k, v]) => ({
                    name: this.dateFormat.format(new Date(k)),
                    value: v.filter(x => !this.selectedArea || this.selectedArea === x[3]).length
                }))
            }
        ];

        let prev = 0;
        this.newCasesCummulativeChartData = [
            {
                name: 'Případy celkem',
                series: Object.entries(this.data.data).map(([k, v]) => {
                    const curr = v.filter(x => !this.selectedArea || this.selectedArea === x[3]).length + prev;
                    prev = curr;

                    return {
                        name: this.dateFormat.format(new Date(k)),
                        value: curr
                    };
                })
            }
        ];
        this.newCasesCummulativeLogaritmicChartData = [
            {
                name: 'Případy celkem (Log10)',
                series: Object.entries(this.data.data).map(([k, v]) => {
                    const val = Math.log10(v.filter(x => !this.selectedArea || this.selectedArea === x[3]).length);
                    const curr = val + prev;
                    prev = curr;

                    return {
                        name: this.dateFormat.format(new Date(k)),
                        value: curr
                    };
                })
            }
        ];
    }

    ngOnInit() {
        this.http
            .get<IApiResponse>('https://api.apify.com/v2/key-value-stores/qAEsnylzdjhCCyZeS/records/LATEST?disableRedirect=true')
            .subscribe(result => {
                this.data = result;
                Object.keys(result.data).forEach(k => {
                    result.data[k].forEach(x => {
                        const [age, gender, infectionSource, area] = x;
                        this.areas[area] = !this.areas[area] ? 1 : this.areas[area] + 1;
                        this.age[age] = !this.age[age] ? 1 : this.age[age] + 1;
                        this.gender[gender] = !this.gender[gender] ? 1 : this.gender[gender] + 1;
                        this.infectionSources[infectionSource] = !this.infectionSources[infectionSource]
                            ? 1
                            : this.infectionSources[infectionSource] + 1;
                    });
                });
                this.areasChartData = Object.entries(this.areas)
                    .map(([k, v]) => ({ name: k, value: v }))
                    .sort((a, b) => b.value - a.value);
                this.infectionSourceChartData = Object.entries(this.infectionSources)
                    .map(([k, v]) => ({ name: k, value: v }))
                    .sort((a, b) => b.value - a.value);
                this.ageChartData = Object.entries(this.age)
                    .map(([k, v]) => ({ name: k, value: v }))
                    .sort((a, b) => +a.name - +b.name);
                this.genderChartData = Object.entries(this.gender)
                    .map(([k, v]) => ({ name: k, value: v }))
                    .sort((a, b) => b.value - a.value);

                this.setNewCasesArea();
            });
    }
}
