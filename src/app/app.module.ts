import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LineChartModule, BarChartModule } from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
    declarations: [AppComponent],
    imports: [BrowserModule, CommonModule, HttpClientModule, LineChartModule, BrowserAnimationsModule, BarChartModule, FormsModule],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
